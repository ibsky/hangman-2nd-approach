﻿using System;
using System.Linq;

class MainClass
{
    public static void Main(string[] args)
    {

        Console.WriteLine("Zagrajmy w Wisielca. Twoim celem jest odgadnięcie słowa");

        Console.WriteLine("Wpisz słowo do odgadnięcia: ");
        string wordToGuess = Console.ReadLine();

        // check if there are only letters
        bool wordTest = wordToGuess.All(Char.IsLetter);

        while (wordTest == false || wordToGuess.Length == 0)
        {
            Console.WriteLine("Słowo może zawierać tylko litery");
            Console.Write("Wpisz słowo: ");
            wordToGuess = Console.ReadLine();
            wordTest = wordToGuess.All(Char.IsLetter);
        }


        wordToGuess = wordToGuess.ToUpper();

        // hide word to huess
        Console.WriteLine("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" +
                          "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" +
                          "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

        int lives = 5;
        int counter = -1;
        int wordLength = wordToGuess.Length;
        char[] wordToGuessArray = wordToGuess.ToCharArray();
        char[] printArray = new char[wordLength];
        char[] guessedLetters = new char[26];
        int numberStore = 0;
        bool victory = false;

        foreach (char letter in printArray)
        {
            counter++;
            printArray[counter] = '-';
        }

        while (lives > 0)
        {
            counter = -1;
            string printProgress = String.Concat(printArray);
            bool letterFound = false;
            int multiples = 0;

            if (printProgress == wordToGuess)
            {
                victory = true;
                break;
            }

            if (lives > 1)
            {
                Console.WriteLine("Masz {0} żyć", lives);
            }
            else
            {
                Console.WriteLine("Zostało Ci tylko {0} żyć!!", lives);
            }


            Console.WriteLine("Odgadnięte litery: " + printProgress);
            Console.Write("\n\n\n");
            Console.Write("Zgadnij literę: ");
            string playerGuess = Console.ReadLine();

            //test to make sure playered entered a single letter
            bool guessTest = playerGuess.All(Char.IsLetter);

            while (guessTest == false || playerGuess.Length != 1)
            {
                Console.WriteLine("Wpisz pojedynczą literę!");
                Console.Write("Zgadnij literę: ");
                playerGuess = Console.ReadLine();
                guessTest = playerGuess.All(Char.IsLetter);
            }

            playerGuess = playerGuess.ToUpper();
            char playerChar = Convert.ToChar(playerGuess);

            if (guessedLetters.Contains(playerChar) == false)
            {

                guessedLetters[numberStore] = playerChar;
                numberStore++;

                foreach (char letter in wordToGuessArray)
                {
                    counter++;
                    if (letter == playerChar)
                    {
                        printArray[counter] = playerChar;
                        letterFound = true;
                        multiples++;
                    }

                }

                if (letterFound)
                {
                    Console.WriteLine("Odgadłeś {0} literę: {1}!", multiples, playerChar);
                }
                else
                {
                    Console.WriteLine("Nie ma litery {0}!", playerChar);
                    lives--;
                }
                Console.WriteLine(GallowView(lives));
            }
            else
            {
                Console.WriteLine("Już wcześniej odgadłeś literę: {0}!!", playerChar);
            }


        }


        if (victory)
        {
            Console.WriteLine("\n\nOdgadłeś słowo: {0}", wordToGuess);
            Console.WriteLine("\n\nWygrałeś!!!!!!!!!!!");
        }
        else
        {
            Console.WriteLine("\n\nNie odgadłeś słowa: {0}", wordToGuess);
            Console.WriteLine("\n\nPrzegrałeś!!!!!!!!!");
        }

    }


    private static string GallowView(int livesLeft)
    {
        //function to print out hangman

        string drawHangman = "";

        if (livesLeft < 5)
        {
            drawHangman += "--------\n";
        }

        if (livesLeft < 4)
        {
            drawHangman += "       |\n";
        }

        if (livesLeft < 3)
        {
            drawHangman += "       O\n";
        }

        if (livesLeft < 2)
        {
            drawHangman += "      /|\\ \n";
        }

        if (livesLeft == 0)
        {
            drawHangman += "      / \\ \n";
        }

        return drawHangman;

    }




}